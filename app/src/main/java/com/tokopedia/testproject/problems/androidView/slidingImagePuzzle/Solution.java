package com.ali.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.ali.myapplication.network.NewsDataSource;
import com.ali.myapplication.network.NewsService;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;

public class Solution {
    public interface onSuccessLoadBitmap {
        void onSliceSuccess(List<Bitmap> bitmapList);

        void onSliceFailed(Throwable throwable);
    }

    public void Solution(){

    }

    public static void sliceTo4x4(Context context, onSuccessLoadBitmap onSuccessLoadBitmap, String imageUrl) {
//        ArrayList<Bitmap> bitmapList = new ArrayList<>();
        // TODO, download the image, crop, then sliced to 15 Bitmap (4x4 Bitmap). ignore the last Bitmap
        // below is stub, replace with your implementation!
//        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample11));
//        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample12));
//        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample13));
//        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample14));
//        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample21));
//        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample22));
//        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample23));
//        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample24));
//        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample31));
//        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample32));
//        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample33));
//        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample34));
//        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample41));
//        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample42));
//        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample43));
//        onSuccessLoadBitmap.onSliceSuccess(bitmapList);
            getImage(imageUrl, onSuccessLoadBitmap);
    }

    private static void getImage(String url, onSuccessLoadBitmap onSuccessLoadBitmap) {
        NewsService network = NewsDataSource.getService();
        network.getImage(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observable(onSuccessLoadBitmap));
    }

    private static DisposableObserver<Response<ResponseBody>> observable(final onSuccessLoadBitmap onSuccessLoadBitmap) {
        return new DisposableObserver<Response<ResponseBody>>() {
            @Override
            public void onNext(Response<ResponseBody> responseBodyResponse) {
                if (responseBodyResponse.body() != null) {
                    Bitmap bitmap = BitmapFactory.decodeStream(responseBodyResponse.body().byteStream());
                    ArrayList<Bitmap> bitmaps = createBitmaps(bitmap);
                    onSuccessLoadBitmap.onSliceSuccess(bitmaps);
                }
            }

            @Override
            public void onError(Throwable e) {
                onSuccessLoadBitmap.onSliceFailed(e);
            }

            @Override
            public void onComplete() {

            }
        };
    }

    private static ArrayList<Bitmap> createBitmaps(Bitmap bitmap) {
        int dimension = 4;
        int height = bitmap.getHeight() / dimension;
        int width = bitmap.getWidth() / dimension;
        ArrayList<Bitmap> bitmaps = new ArrayList<>();
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                if (i == 3 && j == 3) {
                    break;
                } else {
                    bitmaps.add(Bitmap.createBitmap(bitmap, j * width, i * height, width, height));
                }
            }
        }
        return bitmaps;
    }
}
