package com.tokopedia.testproject.problems.androidView.slidingImagePuzzle

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.widget.Button
import android.widget.GridLayout
import android.widget.ImageView
import android.widget.Toast
import java.util.*
import kotlin.collections.ArrayList


class SlidingImageGameActivity : AppCompatActivity() {
    fun getIntent(context: Context, imageUrl: String): Intent {
        val intent = Intent(this, SlidingImageGameActivity::class.java)
        intent.putExtra(IMAGE_URL, imageUrl)
        return intent
    }

    private lateinit var mImg: Button
    private lateinit var mGridLayout: GridLayout
    private var mImageUrl = ""
    private val mShuffleImageViews = arrayOfNulls<ImageView?>(16)
    private val mDrawables = arrayOfNulls<Drawable>(16)
    private val mTempDrawables = arrayOfNulls<Drawable>(16)
    private val mSize = GRID_NO * GRID_NO

    companion object {
        const val TAG = "SlidingImage"
        const val IMAGE_URL = "image"
//        const val URL = "https://i.imgur.com/BxjSKC3.jpg"
        const val GRID_NO = 4
        const val STORE_DRAWABLE = "drawable"
        const val STORE_TEMP_DRAWABLE = "tempDrawable"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mImageUrl = intent.getStringExtra(IMAGE_URL)
//        mImageUrl = URL
        bindView()
        mGridLayout.columnCount = GRID_NO
        mGridLayout.rowCount = GRID_NO

        if (savedInstanceState?.getSerializable(STORE_DRAWABLE) != null && savedInstanceState.getSerializable(
                        STORE_TEMP_DRAWABLE) != null) {
            getDataAfterRotate(savedInstanceState)
        } else {
            getImage()
        }
        findViewById<Button>(R.id.img).setOnClickListener {
            startActivity(SlidingImageGameActivity().getIntent(this, URL))
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putSerializable(STORE_DRAWABLE, mDrawables)
        outState?.putSerializable(STORE_TEMP_DRAWABLE, mTempDrawables)
    }

    private fun bindView() {
        mImg = findViewById(R.id.img)
        mGridLayout = findViewById(R.id.gridLayout)
    }

    private fun createImageView(): ImageView {
        val inflater = LayoutInflater.from(this)
        return inflater.inflate(
                R.layout.item_image_sliding_image,
                mGridLayout, false
        ) as ImageView
    }

    private fun getDataAfterRotate(savedInstanceState: Bundle?) {
        val tempDrawable = savedInstanceState?.getSerializable(STORE_TEMP_DRAWABLE) as Array<Drawable>
        val drawable = savedInstanceState.getSerializable(STORE_DRAWABLE) as Array<Drawable>
        for (i in 0 until mSize) {
            mTempDrawables[i] = tempDrawable[i]
            mDrawables[i] = drawable[i]
            mShuffleImageViews[i] = createImageView()
            mShuffleImageViews[i]?.setImageDrawable(mTempDrawables[i])
        }
        addShuffleImageViewToGridLayout()
        setOnClickListener()
    }


    private fun getImage() {
        Solution.sliceTo4x4(this, object : Solution.onSuccessLoadBitmap {
            override fun onSliceSuccess(bitmapList: MutableList<Bitmap>?) {
                for (i in 0 until mSize) {
                    mShuffleImageViews[i] = createImageView()
                    if (i < 15) {
                        mShuffleImageViews[i]?.setImageBitmap(bitmapList?.get(i))
                    }
                    mDrawables[i] = mShuffleImageViews[i]?.drawable
                }
                shuffleImageView()
                setOnClickListener()
            }

            override fun onSliceFailed(throwable: Throwable?) {
                Toast.makeText(this@SlidingImageGameActivity, throwable?.message, Toast.LENGTH_SHORT).show()
            }
        }, mImageUrl)
    }

    private fun shuffleImageView() {
        val random = Random()
        var counter = random.nextInt(15)
        val numbers: ArrayList<Int> = ArrayList()
        for (i in mShuffleImageViews.size - 1 downTo 0) {
            while (numbers.contains(counter) && numbers.size <= mShuffleImageViews.size) {
                counter = random.nextInt(mShuffleImageViews.size)
                if (numbers.size == mShuffleImageViews.size)
                    break
            }
            numbers.add(counter)
            val temp = mShuffleImageViews[counter]
            mShuffleImageViews[counter] = mShuffleImageViews[i]
            mShuffleImageViews[i] = temp
            mTempDrawables[counter] = mShuffleImageViews[counter]?.drawable
            mTempDrawables[i] = temp?.drawable
        }
        addShuffleImageViewToGridLayout()
    }

    private fun swapPosition(newPosition: Int, currentPosition: Int) {
        mShuffleImageViews[newPosition]!!.setImageDrawable(mShuffleImageViews[currentPosition]!!.drawable)
        mGridLayout.removeViewAt(currentPosition)
        mGridLayout.addView(mShuffleImageViews[currentPosition], currentPosition)
        mShuffleImageViews[currentPosition]!!.setImageDrawable(null)
        mTempDrawables[newPosition] = mShuffleImageViews[newPosition]?.drawable
        mTempDrawables[currentPosition] = mShuffleImageViews[currentPosition]?.drawable
    }

    private fun addShuffleImageViewToGridLayout() {
        if (mGridLayout.parent != null) {
            mGridLayout.removeAllViews()
        }
        for (i in 0 until mSize) {
            mGridLayout.addView(mShuffleImageViews[i])
        }
    }

    private fun setOnClickListener() {
        for (i in 0 until mShuffleImageViews.size) {
            mShuffleImageViews[i]!!.setOnClickListener {
                val top: Int? = if (i - GRID_NO >= 0) i - GRID_NO else null
                val bottom: Int? = if (i + GRID_NO < mShuffleImageViews.size) i + GRID_NO else null
                val left: Int? = if (i % GRID_NO > 0) i - 1 else null
                val right: Int? = if (i % GRID_NO < GRID_NO - 1) i + 1 else null

                Log.i(
                        "posisi",
                        mShuffleImageViews.size.toString() + " " + i + " " + top + " " + right + " " + left + " " + bottom
                )
                if (top != null && mShuffleImageViews[top]!!.drawable == null) {
                    swapPosition(top, i)
                } else if (bottom != null && mShuffleImageViews[bottom]!!.drawable == null) {
                    swapPosition(bottom, i)
                } else if (left != null && mShuffleImageViews[left]!!.drawable == null) {
                    swapPosition(left, i)
                } else if (right != null && mShuffleImageViews[right]!!.drawable == null) {
                    swapPosition(right, i)
                }
                if (isSolved()) {
                    Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show()
                    buildDialog()
                }
            }
        }
    }

    private fun buildDialog() {
        val builder = AlertDialog.Builder(this)
        val builderCreate = builder.create()
        builder.setTitle("Completed")
        builder.setMessage("Puzzle is completed")
        builder.setPositiveButton(android.R.string.ok) { _, _ ->
        }
        builder.setNegativeButton(android.R.string.cancel) { _, _ ->
            builderCreate.dismiss()
        }
        builder.setCancelable(false)
        builder.show()
    }

    private fun isSolved(): Boolean {
        var solved = true
        for (i in 0 until GRID_NO) {
            for (j in 0 until GRID_NO) {
                val index: Int = i * GRID_NO + j
                if (mDrawables[index] == mShuffleImageViews[index]!!.drawable) {
                    Log.i(TAG, "$index ${mShuffleImageViews[index]!!.drawable}")
                    continue
                } else if (mShuffleImageViews[index]!!.drawable != mDrawables[index]) {
                    solved = false
                    break
                }
            }
        }
        return solved
    }
}
