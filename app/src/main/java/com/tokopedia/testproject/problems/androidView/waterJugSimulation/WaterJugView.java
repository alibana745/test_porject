package com.tokopedia.testproject.problems.androidView.waterJugSimulation;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;


public class WaterJugView extends View {

    private Paint mPaint;
    private Path mPath;
    private float minY = 100f;
    private float maxY = 0f;
    private float minX = 250f;
    private float maxX = 0f;
    private float waterFill = 0;
    private float maxWater = 0;

    public WaterJugView(Context context) {
        super(context);
        createJugWater();
    }

    public WaterJugView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createJugWater();
    }

    public WaterJugView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createJugWater();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public WaterJugView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createJugWater();
    }

    public void setMaxWater(int maxWater) {
        this.maxWater = maxWater;
    }

    public void setWaterFill(int waterFill) {
        this.waterFill = waterFill;
    }

    private void createJugWater() {
        mPaint = new Paint();
        mPaint.setColor(Color.BLUE);
        mPaint.setStrokeWidth(10f);
        mPaint.setStyle(Paint.Style.STROKE);
        float radius = 50.0f;
        CornerPathEffect corEffect = new CornerPathEffect(radius);
        mPaint.setPathEffect(corEffect);

        mPath = new Path();
        float x = 250f;
        float y = 100f;
        for (int i = 0; i < 9; i++) {
            if (i < 3) {
                mPath.moveTo(x, y);
                y += 50;
                mPath.lineTo(x, y);
                y += 25;
            } else if (i == 3) {
                x += 50;
                y -= 25;
                maxY = y;
                mPath.lineTo(x, y);
            } else if (i == 4 || i == 5) {
                x += 50;
                mPath.moveTo(x, y);
                x += 50;
                mPath.lineTo(x, y);
            } else if (i == 6) {
                maxX = x;
                y -= 25;
                mPath.lineTo(x, y);
                y -= 50;
            } else {
                mPath.moveTo(x, y);
                y -= 50;
                mPath.lineTo(x, y);
                y -= 25;
            }
        }
        fillWaterToBucket();
    }

    private void fillWaterToBucket() {
        if (waterFill > maxWater) {
            waterFill = maxWater;
        } else if (waterFill <= 0) {
            waterFill = 0;
        }
        generateViewWater();
    }

    private void generateViewWater() {
        if (waterFill > 0 && maxWater > 0) {
            float tempYCoordinat = minX;
            float positionWater = waterPosition();
            for (int i = 0; i< 4; i++) {
                if (i == 3) {
                    mPath.moveTo(tempYCoordinat, positionWater);
                    mPath.lineTo(maxX, positionWater);
                } else {
                    mPath.moveTo(tempYCoordinat, positionWater);
                    mPath.lineTo(tempYCoordinat + 45, positionWater);
                    tempYCoordinat += 60;
                }
            }
        }
    }

    private Float waterPosition() {
        float temp =   waterFill / maxWater;
        float water = temp * 100;
        float bucketPercentage = maxY - minY;
        float result = water * bucketPercentage / 100;
        return maxY - result;
    }
    //TODO
    /*
    Based on these variables: maxWater and waterFill, draw the jug with the water

    Example a:
    maxWater = 10
    waterFill = 0

    Result,
    View will draw like below
    |        |
    |        |
    |        |
    |        |
    `--------'

    Example b:
    maxWater = 10
    waterFill = 5

    Result,
    View will draw like below
    |        |
    |        |
    |--------|
    |        |
    `--------'

    Example c:
    maxWater = 10
    waterFill = 8

    Result,
    View will draw like below
    |        |
    |--------|
    |        |
    |        |
    `--------'

    Example d:
    maxWater = 10
    waterFill = 10

    Result,
    View will draw like below
     ________
    |        |
    |        |
    |        |
    |        |
    `--------'
    */

}
