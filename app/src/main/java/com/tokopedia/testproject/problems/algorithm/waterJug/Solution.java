package com.tokopedia.testproject.problems.algorithm.waterJug;

public class Solution {

    public static int minimalPourWaterJug(int jug1, int jug2, int target) {
        // TODO, return the smallest number of POUR action to do the water jug problem
        // below is stub, replace with your implementation!
        int tempA = 0;
        int tempB = 0;
        int counter = 0;
        boolean isTarget = false;
        while (!isTarget) {
            if (tempB == 0) {
                tempB = b;
            }
            if (tempA == a) {
                tempA = 0;
            }
            if (tempB > 0) {
                boolean isDone = false;
                while (!isDone) {
                    tempA++;
                    tempB--;
                    if (tempA == a || tempB == 0) {
                        isDone = true;
                    }
                }
                counter++;
            }
            if (tempA == target || tempB == target) {
                isTarget = true;
            }
        }
        return counter;
    }
}
