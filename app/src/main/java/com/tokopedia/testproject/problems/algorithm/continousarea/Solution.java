package com.tokopedia.testproject.problems.algorithm.continousarea;

/**
 * Created by hendry on 18/01/19.
 */
public class Solution {
    public static int maxContinuousArea(int[][] matrix) {
        // TODO, return the largest continuous area containing the same integer, given the 2D array with integers
        // below is stub
        int counter = 0;
        int tempSameValue = -1;
        for(int i =0;  i < 5; i++){
            for(int j = 0; j < 5 ; j++){
                if(tempSameValue == -1){
                    tempSameValue = matrix[i][j];
                } else {
                    if (tempSameValue == matrix[i][j]){
                        counter +=1;
                    } else {
                        tempSameValue = matrix[i][j];
                        counter = 1;
                    }
                }
            }
        }
        return counter;
    }
}
