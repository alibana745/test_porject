package com.tokopedia.testproject.problems.algorithm.maxrectangle;


public class Solution {
    public static int maxRect(int[][] matrix) {
        // TODO, return the largest area containing 1's, given the 2D array of 0s and 1s
        // below is stub
        int tempCounterOne = 0;
        int tempCounterZero = 0;
        int counterOne = 0;
        int counterZero = 0;

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (matrix[i][j] == 1) {
                    if (tempCounterZero > counterZero) {
                        counterZero = tempCounterZero;
                    }
                    tempCounterZero = 0;
                    tempCounterOne++;
                } else {
                    if (tempCounterOne >= counterOne) {
                        counterOne = tempCounterOne;
                    }
                    tempCounterOne = 0;
                    tempCounterZero++;
                }
            }
        }
        return counterOne;
    }
}
